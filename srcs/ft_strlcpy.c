#include "../include/libft.h"

size_t	ft_strlcpy(char *restrict dst, const char *restrict src, size_t dstsize)
{
	size_t i;
	size_t len;

	if (!dst || !src)
		return (0);
	i = 0;
	while (src[i] && (i + 1 < dstsize))
	{
		dst[i] = src[i];
		i++;
	}
	if (dstsize)
		dst[i] = '\0';
	len = 0;
	while (src[len])
		++len;
	return (len);
}
