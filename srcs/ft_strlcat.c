#include "../include/libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t len;
	size_t i;
	size_t j;

	len = 0;
	i = 0;
	j = 0;
	while (dst[i])
		++i;
	while (src[len])
		++len;
	len += (dstsize <= i) ? dstsize : i;
	while (src[j] && i + 1 < dstsize)
		dst[i++] = src[j++];
	dst[i] = '\0';
	return (len);
}
