# Libft

## Résumé
Ce projet a pour objectif de nous faire coder en C une librairie de fonctions usuelles que nous pourrons utiliser dans tous nos prochains projets.

Skills:
- Imperative programming
- Rigor
- Algorithms & AI 

Lire le [sujet][1].

`Ce projet a été codé sur Macos`

### Compiler et lancer le projet

1. Téléchargez / Clonez le dépot

```
git clone https://gitlab.com/flmarsil42/libft && cd libft
```

2. Compilez le projet et utilisez le fichier libft.a avec votre main

```
Make
gcc -Wall -Werror -Wextra main.c libft.a
```

[1]:https://gitlab.com/flmarsil42/libft/-/blob/master/fr.libftsubject.pdf
