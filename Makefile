CC = gcc -Wall -Werror -Wextra
SRCS =  ./srcs/ft_atoi.c		\
        ./srcs/ft_bzero.c		\
        ./srcs/ft_calloc.c		\
        ./srcs/ft_isalnum.c		\
        ./srcs/ft_isalpha.c		\
       	./srcs/ft_isascii.c		\
        ./srcs/ft_isdigit.c		\
        ./srcs/ft_isprint.c		\
		./srcs/ft_itoa.c		\
        ./srcs/ft_memccpy.c		\
        ./srcs/ft_memchr.c		\
        ./srcs/ft_memcmp.c		\
        ./srcs/ft_memcpy.c		\
        ./srcs/ft_memmove.c		\
        ./srcs/ft_memset.c		\
        ./srcs/ft_putchar_fd.c	\
		./srcs/ft_putendl_fd.c	\
		./srcs/ft_putnbr_fd.c	\
    	./srcs/ft_putstr_fd.c	\
        ./srcs/ft_split.c		\
        ./srcs/ft_strchr.c		\
        ./srcs/ft_strdup.c		\
        ./srcs/ft_strjoin.c		\
        ./srcs/ft_strlcat.c		\
        ./srcs/ft_strlcpy.c		\
        ./srcs/ft_strlen.c		\
		./srcs/ft_strmapi.c		\
        ./srcs/ft_strncmp.c		\
        ./srcs/ft_strnstr.c		\
		./srcs/ft_strnew.c		\
        ./srcs/ft_strrchr.c		\
        ./srcs/ft_strtrim.c		\
        ./srcs/ft_substr.c		\
        ./srcs/ft_tolower.c		\
        ./srcs/ft_toupper.c		
SRCS_BONUS =	./srcs_bonus/ft_lstnew.c		\
	        	./srcs_bonus/ft_lstsize.c		\
				./srcs_bonus/ft_lstadd_front.c	\
				./srcs_bonus/ft_lstadd_back.c	\
	    		./srcs_bonus/ft_lstclear.c		\
	        	./srcs_bonus/ft_lstdelone.c		\
	        	./srcs_bonus/ft_lstlast.c		\
	        	./srcs_bonus/ft_lstiter.c		\
	        	./srcs_bonus/ft_lstmap.c
OBJS	=	${SRCS:.c=.o}
OBJS_BONUS	= ${SRCS_BONUS:.c=.o}
HEADERS	= ./include
RM		= rm -f
NAME	= libft.a
all:	${NAME}
bonus:		${OBJS} ${OBJS_BONUS}
			ar -rc ${NAME} $^
			ranlib ${NAME}
$(NAME):	${OBJS}
			ar -rc ${NAME} $^
			ranlib ${NAME}
%.o:%.c
	${CC} -o $@ -c $<
clean	:
			${RM} ${OBJS} ${OBJS_BONUS}
fclean	: clean
			${RM} ${NAME}
re		: fclean all
